<!-- summernote text plugin-->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script> 

<!-- include summernote css/js -->
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js"></script>

  <div class="container">
  <div class="card-deck">
  <div class="card">
  <form action="" method="post">
  <input  type="hidden" name="id_notulen" value="<?= $notulen['id_notulen']; ?>">
  <div class="card-header" style="text-align:center">
    Isi Notulen Rapat
  </div>
  <p class="card-title">
  <div class="container-fluid" align="center">
  <textarea margin="auto" type="text" name="hasil_record" class="form-control" id="hasil_record_read"><?= $notulen['hasil_record']; ?></textarea>
  </p>
  </div>
  <div class="container">
  <a href="<?= base_url(); ?>notulen/cetak/<?= $notulen['id_notulen']?>" class="btn btn-primary" >Print this page</button>
  <a href="<?= base_url(); ?>notulen/submenu" class="btn btn-primary float-right">Kembali</a>
  </div>
  </div>
  </div>
  </div>

  <script>
        $('#hasil_record_read').summernote({
          toolbar: [
        // [groupName, [list of button]]
  ],
        height: 810,
        width:770
      });
      
  $('body > .note-table-popover,.note-image-popover,.note-link-popover').hide();    
  $(".note-editable").attr("contenteditable","false");
  $(".note-editable").attr("style","text-align:justify;");
  $(".table-bordered").attr("border","1");
  $(".table-bordered").attr("cellspacing","0");

  </script>

<div class="card" style="width: 500px">
  <div class="card-header" style="text-align:center">
    Informasi Rapat
  </div>
  <div class="card-body" style="text-align:center">
  <b><b>Nomor Rapat:</b>
  <p class="card-title"><?= $notulen['nomor_rapat']; ?></p>
  </b>
  <b><b>Judul Rapat:</b>
  <p class="card-title"><?= $notulen['judul_rapat']; ?></p>
  </b>
  <b><b>Nama Pemimpin Rapat:</b>
  <p class="card-title"><?= $notulen['nama_user']; ?></p>
  </b>
  <b><b>Ruang Rapat:</b>
  <p class="card-title"><?= $notulen['nama_ruang']; ?></p>
  </b>
  <b><b>Tanggal Rapat:</b>
  <p class="card-title"><?= $notulen['tanggal']; ?></p>
  </b>
  <b><b>Jam Mulai Rapat:</b>
  <p class="card-title"><?= $notulen['jam_mulai']; ?></p>
  </b>
  <b><b>Jam Selesai Rapat:</b>
  <p class="card-title"><?= $notulen['jam_selesai']; ?></p>
  </b>
  <b><b>Presensi:</b>
  <p class="card-title"><?= $notulen['absen']; ?></p>
  </b>
  </div>
  </div>
    </div>
    <!-- <p align="center">Mohon untuk melakukan pengeditan kembali, jika ada "tabel" yang tidak tampil pada saat melakukan print</p> -->