<div class="container">
<div id="accordion">
  <div class="card">
    <div class="card-header" id="headingOne">
    <h5 class="mb-0">
        <a class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
        Form Tambah Data Notulen
        </a>
      </h5>
    </div>

    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
      <div class="card-body">
        <div class="col-md-9">
        <div class="card-body">
            <form action="" method="post">
                <input type="hidden" name="id_notulen">
                <div class="form-group">
                    <label for="nomor_rapat">Nomor Rapat</label>
                    <input style="width: 770px" type="text" name="nomor_rapat" class="form-control" id="nomor_rapat">
                    <small class="form-text text-danger"><?= form_error('nomor_rapat'); ?></small>
                </div>
                <div class="form-group">
                    <label for="tanggal">Hari/Tanggal</label>
                    <input style="width: 770px" type="text" name="tanggal" class="form-control" id="tanggal">
                    <small class="form-text text-danger"><?= form_error('tanggal'); ?></small>
                </div>
                <div class="form-group">
                    <label for="judul_rapat">Judul Rapat</label>
                    <input style="width: 770px" type="text" name="judul_rapat" class="form-control" id="judul_rapat">
                    <small class="form-text text-danger"><?= form_error('judul_rapat'); ?></small>
                </div>
                <div class="form-group">
                    <label for="nama_user">Pemimpin Rapat</label>
                    <select style="width: 770px" class="form-control" id="nama_user" name="nama_user">
                            <option value="">Pilih Pemimpin Rapat</option>
                            <?php foreach ($menu as $m) : ?>
                            <option value="<?= $m['name']; ?>"><?= $m['name'] ?></option>
                            <?php endforeach; ?>
                        </select>
                        <small class="form-text text-danger"><?= form_error('nama_user'); ?></small>
                </div>
                <div class="form-group">
                    <label for="nama_ruang">Ruangan</label>
                        <select style="width: 770px" name="nama_ruang" id="nama_ruang" class="form-control">
                            <option value="">Pilih Ruangan</option> 
                            <option value="Ruang Dosen">Ruang Dosen</option>
                            <option value="Ruang Rapat">Ruang Rapat</option>        
                        </select>
                        <small class="form-text text-danger"><?= form_error('nama_ruang'); ?></small>
                    </div>
                <div class="form-group">
                    <label for="jam_mulai">Jam Mulai</label>
                    <input style="width: 770px" type="time" name="jam_mulai" class="form-control" id="jam_mulai">
                    <small class="form-text text-danger"><?= form_error('jam_mulai'); ?></small>
                </div>
                <div class="form-group">
                    <label for="jam_selesai">Jam Selesai</label>
                    <input style="width: 770px" type="time" name="jam_selesai" class="form-control" id="jam_selesai">
                    <small class="form-text text-danger"><?= form_error('jam_selesai'); ?></small>
                </div>
                <div class="form-group">
                    <label for="absen">Presensi</label>
                    <textarea class="form-control rounded-0" id="absentambah" rows="10" name="absen"></textarea>
                    <small class="form-text text-danger"><?= form_error('absen'); ?></small>
                </div>
                  </div>
                </div>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingTwo" style="text-align:left">
      <h5 class="mb-0">
        <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
        Form Edit Isi Notulen
        </a>
      </h5>
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
      <div class="card-body" align="center">
                        <div class="col-md-10">
                        <div class="form-group">
                            <label for="hasil_record"></label>
                            <textarea margin="auto" type="text" name="hasil_record" class="form-control" id="hasil_record"></textarea>
                            <small class="form-text text-danger"><?= form_error('hasil_record'); ?></small>
                        </div>
                        </div>
                </div>
            </div>
      </div>
    </div>
    <br>
    <button type="submit" name="tambah" class="btn btn-primary float-right">Tambah Data</button>
    <a href="<?= base_url(); ?>notulen/submenu" class="btn btn-primary float-left">Kembali</a>
    </form>
  </div>
    </div>
  </div>
</div>
