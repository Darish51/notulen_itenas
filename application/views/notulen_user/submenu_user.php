<div class="container mt-5">


    <div class="row">
        <div class="col-6">


            <!-- Begin Page Content -->
            <div class="container-fluid">

                <!-- Page Heading -->
                <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>



                <div class="row">
                    <div class="col-lg">
                        <?php if (validation_errors()) : ?>
                            <div class="alert alert-danger" role="alert">
                                <?= validation_errors(); ?>
                            </div>
                        <?php endif; ?>

                        <?= $this->session->flashdata('message'); ?>

                        <table class="table table-hover" id="datatables">
                            <thead>
                                <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Nomor Rapat</th>
                                    <th scope="col">Judul Rapat</th>
                                    <th scope="col">Pemimpin Rapat</th>
                                    <th scope="col">Ruang Rapat</th>
                                    <th scope="col">Tanggal</th>
                                    <th scope="col">Jam Mulai</th>
                                    <th scope="col">Jam Selesai</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1; ?>
                                <?php foreach ($subMenu as $sm) : ?>
                                    <tr>
                                        <th scope="row"><?= $i; ?></th>
                                        <td><?= $sm['nomor_rapat']; ?></td>
                                        <td><?= $sm['judul_rapat']; ?></td>
                                        <td><?= $sm['nama_user']; ?></td>
                                        <td><?= $sm['nama_ruang']; ?></td>
                                        <td><?= $sm['tanggal']; ?></td>
                                        <td><?= $sm['jam_mulai']; ?></td>
                                        <td><?= $sm['jam_selesai']; ?></td>
                                        <td>
                                            <a href="<?= base_url(); ?>notulen_user/detail/<?= $sm['id_notulen'] ?>" class="badge badge-primary">detail</a>
                                        </td>
                                    </tr>
                                    <?php $i++; ?>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>


        <!-- Modal -->
        <!-- <div class="modal fade" id="newSubMenuModal" tabindex="-1" role="dialog" aria-labelledby="newSubMenuModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="newSubMenuModalLabel">Add New Notulen</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="<?= base_url('notulen/submenu'); ?>" method="post">
                        <input type="hidden" class="form-control" id="id_notulen" name="id_notulen" placeholder="Id Notulen">
                        <div class="modal-body">
                            <div class="form-group">
                                <input type="text" class="form-control" id="nomor_rapat" name="nomor_rapat" placeholder="Nomor Rapat">
                                <small class="form-text text-danger"><?= form_error('nomor_rapat'); ?></small>
                            </div>
                            <div class="form-group">
                                <input type="date" class="form-control" id="tanggal" name="tanggal" placeholder="Tanggal">
                                <small class="form-text text-danger"><?= form_error('tanggal'); ?></small>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="judul_rapat" name="judul_rapat" placeholder="Judul Rapat">
                                <small class="form-text text-danger"><?= form_error('judul_rapat'); ?></small>
                            </div>
                            <div class="form-group">
                                <select name="nama_user" id="nama_user" class="form-control">
                                    <option value="">Pilih Pemimpin Rapat</option>
                                    <?php foreach ($menu as $m) : ?>
                                        <option value="<?= $m['name']; ?>"><?= $m['name'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <small class="form-text text-danger"><?= form_error('nama_user'); ?></small>
                            </div>
                            <div class="form-group">
                                <select name="nama_ruang" id="nama_ruang" class="form-control">
                                    <option value="">Pilih Ruangan</option>
                                    <option value="Ruang Dosen">Ruang Dosen</option>
                                    <option value="Ruang Rapat">Ruang Rapat</option>
                                </select>
                                <small class="form-text text-danger"><?= form_error('nama_ruang'); ?></small>
                            </div>
                            <div class="form-group">
                                <input type="time" class="form-control" id="jam_mulai" name="jam_mulai" placeholder="Jam Mulai">
                                <small class="form-text text-danger"><?= form_error('jam_mulai'); ?></small>
                            </div>
                            <div class="form-group">
                                <input type="time" class="form-control" id="jam_selesai" name="jam_selesai" placeholder="Jam Selesai">
                                <small class="form-text text-danger"><?= form_error('jam_selesai'); ?></small>
                            </div>
                            <div class="form-group">
                                <textarea type="text" class="form-control" id="hasil_record" name="hasil_record" placeholder="Isi Rapat"></textarea>
                                <small class="form-text text-danger"><?= form_error('hasil_record'); ?></small>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="Submit" name="ubah" class="btn btn-primary">Add</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div> -->