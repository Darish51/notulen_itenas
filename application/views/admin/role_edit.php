<div class="container" align="center">
        <div class="card col-md-14">
            <div class="card-header">
                <h5 class="mb-0">
                    <a class="btn btn-link">
                        Form Edit User Role
                    </a>
                </h5>
            </div>

                <div class="card-body">
                    <div class="col-md-6">
                        <br>
                            <form action="" method="post">
                            <p><b>1 = Administrator</b></p>
                            <p><b>2 = User</b></p>
                            <br>
                                <input type="hidden" name="id" value="<?= $user['id']; ?>">
                                <div class="form-group">
                                    <label for="role_id">Role ID</label>
                                    <select class="form-control" id="role_id" name="role_id">
                                     <?php foreach ($role as $m) : ?>
                                     <?php if( $m['id'] == $user['role_id'] ) : ?>
                                      <option value="<?= $m['id']; ?>" selected><?= $m['id']; ?></option>
                                        <?php else : ?>
                                     <option value="<?= $m['id']; ?>"><?= $m['id'] ?></option>
                                     <?php endif; ?>
                                     <?php endforeach; ?>
                                     </select>
                                    <small class="form-text text-danger"><?= form_error('role_id'); ?></small>
                                    <br>
                                </div>
                                <button type="submit" name="role_edit" class="btn btn-primary float-right">Ubah Role</button>
                            </form>
                            <a href="<?= base_url(); ?>admin/role_user" class="btn btn-primary float-left">Kembali</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>