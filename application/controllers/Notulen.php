<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Notulen extends CI_Controller
{
    private $table = 'notulen';
    public $db;

    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->load->model('Menu_model');
    }

    public function tambah()
    {
        $data['title'] = 'Tambah Data Mahasiswa';

        $this->load->model('Menu_model', 'menu');
        $data['menu'] = $this->db->get('user')->result_array();

        $this->form_validation->set_rules('nomor_rapat', 'Nomor Rapat', 'required');
        $this->form_validation->set_rules('judul_rapat', 'Judul Rapat', 'required');
        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
        $this->form_validation->set_rules('nama_user', 'Pemimpin Rapat', 'required');
        $this->form_validation->set_rules('nama_ruang', 'Ruangan', 'required');
        $this->form_validation->set_rules('jam_mulai', 'Jam Mulai', 'required');
        $this->form_validation->set_rules('jam_selesai', 'Jam Selesai', 'required');
        $this->form_validation->set_rules('absen', 'Absen', 'required');
        $this->form_validation->set_rules('hasil_record', 'Isi Notulen Rapat');

        if ($this->form_validation->run() == false) {
            // $this->load->view('templates/sidebar', $data);
            // $this->load->view('templates/topbar', $data);
            $this->load->view('templates/header', $data);
            $this->load->view('notulen/tambah', $data);
            $this->load->view('templates/footer');
        } else {
            $this->Menu_model->tambahDataNotulen();
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Data Notulen Added!</div>');
            redirect('notulen/submenu');
    }
}

    public function submenu()
    {
        $data['title'] = 'Notulen Management';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $this->load->model('Menu_model', 'menu');

        $data['subMenu'] = $this->menu->getSubMenu1();
        // $data['menu'] = $this->db->get('user')->result_array();

        // $this->form_validation->set_rules('nomor_rapat', 'Nomor Rapat', 'required');
        // $this->form_validation->set_rules('judul_rapat', 'Judul Rapat', 'required');
        // $this->form_validation->set_rules('tanggal', 'tanggal', 'required');
        // $this->form_validation->set_rules('nama_user', 'Pemimpin Rapat', 'required');
        // $this->form_validation->set_rules('nama_ruang', 'Ruangan', 'required');
        // $this->form_validation->set_rules('jam_mulai', 'Jam Mulai', 'required');
        // $this->form_validation->set_rules('jam_selesai', 'Jam Selesai', 'required');
        // $this->form_validation->set_rules('hasil_record', 'Isi Notulen Rapat');

        // if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('notulen/submenu', $data);
            $this->load->view('templates/footer');
        // } else {
            // $data = [
            //     'nomor_rapat' => $this->input->post('nomor_rapat'),
            //     'judul_rapat' => $this->input->post('judul_rapat'),
            //     'tanggal' => $this->input->post('tanggal'),
            //     'nama_user' => $this->input->post('nama_user'),
            //     'nama_ruang' => $this->input->post('nama_ruang'),
            //     'jam_mulai' => $this->input->post('jam_mulai'),
            //     'jam_selesai' => $this->input->post('jam_selesai'),
            //     'hasil_record' => $this->input->post('hasil_record')

            // ];
            // $this->db->insert('notulen', $data);
            // $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">New Notulen Added!</div>');
            // redirect('notulen/submenu');
        // }
    }

    public function hapus($id)
    {

        $this->Menu_model->hapusDataNotulen($id);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Data Notulen Deleted!</div>');
        redirect('notulen/submenu');
    }

    public function detail($id)
    {
        $data['Judul'] = 'Detail data Notulen';
        $data['notulen'] = $this->Menu_model->getNotulenById($id);
        $this->load->view('templates/header', $data);
        $this->load->view('notulen/detail', $data);
        $this->load->view('templates/footer');

    }

    public function cetak($id)
    {
        $data['Judul'] = 'Cetak data Notulen';
        $data['notulen'] = $this->Menu_model->getNotulenById($id);
        $this->load->view('templates/header', $data);
        $this->load->view('notulen/cetak', $data);
        $this->load->view('templates/footer');

    }

    
    //     public function getubah()
    // {
    //     echo json_encode($this->model('Menu_model')->getNotulenById($_POST['id_notulen']));
    // }

    // public function ubah()
    // {
    //     if( $this->model('Menu_model')->ubahDataNotulen($_POST) > 0 ) {
    //         Flasher::setFlash('berhasil', 'diubah', 'success');
    //         header('Location: ' . BASEURL . '/notulen/submenu');
    //         exit;
    //     } else {
    //         Flasher::setFlash('gagal', 'diubah', 'danger');
    //         header('Location: ' . BASEURL . '/notulen/submenu');
    //         exit;
    //     } 
    // }

    public function ubah($id)
    {
        $data['Judul'] = 'Form Ubah Data Notulen';
        $data['ruang'] = ['Ruang Dosen', 'Ruang Rapat'];

        $data['menu'] = $this->db->get('user')->result_array();

        $data['notulen'] = $this->Menu_model->getNotulenById($id);

        $this->form_validation->set_rules('nomor_rapat', 'Nomor Rapat', 'required');
        $this->form_validation->set_rules('judul_rapat', 'Judul Rapat', 'required');
        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
        $this->form_validation->set_rules('nama_user', 'Pemimpin Rapat', 'required');
        $this->form_validation->set_rules('nama_ruang', 'Ruangan', 'required');
        $this->form_validation->set_rules('jam_mulai', 'Jam Mulai', 'required');
        $this->form_validation->set_rules('jam_selesai', 'Jam Selesai', 'required');
        $this->form_validation->set_rules('absen', 'Absen', 'required');
        $this->form_validation->set_rules('hasil_record', 'Isi Notulen Rapat', 'required');

        if ($this->form_validation->run() == false) {
            // $this->load->view('templates/sidebar', $data);
            // $this->load->view('templates/topbar', $data);
            $this->load->view('templates/header', $data);
            $this->load->view('notulen/ubah', $data);
            $this->load->view('templates/footer');
        } else {
            $this->Menu_model->ubahDataNotulen();
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Data Notulen Changed!</div>');
            redirect('notulen/submenu');
        }
    }

}
