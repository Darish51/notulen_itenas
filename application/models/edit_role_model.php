<?php
defined('BASEPATH') or exit('No direct script access allowed');

class edit_role_model extends CI_Model
{
    public $table = 'user';
    public $id = 'id';

    function __construct()
    {
        parent::__construct();
    }


    public function getRole()
    {
        $query = "SELECT * from user";

        return $this->db->query($query)->result_array();
    }

    public function getAllUser()
    {
        return $this->db->get('user')->result_array();
    }


    public function getUserById($id)
    {
        return $this->db->get_where('user', ['id' => $id])->row_array();
    }

    public function ubahDataRole()
    {
        $data = [

            "role_id" => $this->input->post('role_id', false),
        ];

        $this->db->where('id', $this->input->post('id'));
        $this->db->update('user', $data);
    }
}
