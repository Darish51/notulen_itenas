<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Menu_model extends CI_Model
{
    public $table = 'notulen';
    public $id = 'id_notulen';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }
    public function getSubMenu()
    {
        $query = "SELECT `user_sub_menu`.*, `user_menu`.`menu`
                  FROM `user_sub_menu` JOIN `user_menu`
                  ON `user_sub_menu`.`menu_id` = `user_menu`.`id`
                 ";

        return $this->db->query($query)->result_array();
    }

    public function getSubMenu1()
    {
        $query = "SELECT * from notulen";

        return $this->db->query($query)->result_array();
    }

    public function getAllNotulen()
    {
        return $this->db->get('notulen')->result_array();
    }

    public function hapusDataNotulen($id)
    {
        $this->db->delete('notulen', ['id_notulen' => $id]);
    }

    public function getNotulenById($id)
    {
        return $this->db->get_where('notulen', ['id_notulen' => $id])->row_array();
    }

    function total_rows($q = NULL)
    {
        $this->db->like('id_notulen', $q);
        $this->db->or_like('nomor_rapat', $q);
        $this->db->or_like('judul_rapat', $q);
        $this->db->or_like('tanggal', $q);
        $this->db->or_like('nama_user', $q);
        $this->db->or_like('nama_ruang', $q);
        $this->db->or_like('jam_mulai', $q);
        $this->db->or_like('jam_selesai', $q);
        $this->db->or_like('hasil_record', $q);
        $this->db->or_like('absen', $q);
        
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // public function getNotulenById2($id)
    // {
    //     $this->db->query('SELECT * FROM ' . $this->table . ' WHERE id_notulen=:id_notulen');
    //     $this->db->bind('id_notulen', $id);
    //     return $this->db->single();
    // }

    // public function cariDataNotulen()
    // {
    //     $keyword = $this->input->post('keyword', true);
    //     $this->db->like('judul_rapat', $keyword);
    //     $this->db->or_like('nomor_rapat', $keyword);
    //     $this->db->or_like('tanggal', $keyword);
    //     $this->db->or_like('nama_user', $keyword);
    //     $this->db->or_like('nama_ruang', $keyword);
    //     $this->db->or_like('jam_mulai', $keyword);
    //     $this->db->or_like('jam_selesai', $keyword);
    //     $this->db->or_like('hasil_record', $keyword);
    //     return $this->db->get('notulen')->result_array();
    // }

    //     public function ubahDataNotulen($data)
    // {
    //     $query = "UPDATE notulen SET
    //                 judul_rapat = :judul_rapat,
    //                 nomor_rapat = :nomor_rapat,
    //                 tanggal = :tanggal,
    //                 nama_user = :nama_user,
    //                 nama_ruang = :nama_ruang,
    //                 jam_mulai = :jam_mulai,
    //                 jam_selesai = :jam_selesai,
    //                 hasil_record = :hasil_record
    //               WHERE id_notulen = :id_notulen";

    //     $this->db->query($query);
    //     $this->db->bind('judul_rapat', $data['judul_rapat']);
    //     $this->db->bind('nomor_rapat', $data['nomor_rapat']);
    //     $this->db->bind('tanggal', $data['tanggal']);
    //     $this->db->bind('nama_user', $data['nama_user']);
    //     $this->db->bind('nama_ruang', $data['nama_ruang']);
    //     $this->db->bind('jam_mulai', $data['jam_mulai']);
    //     $this->db->bind('jam_selesai', $data['jam_selesai']);
    //     $this->db->bind('hasil_record', $data['hasil_record']);
    //     $this->db->bind('id_notulen', $data['id_notulen']);

    //     $this->db->execute();

    //     return $this->db->rowCount();
    // }

    public function ubahDataNotulen()
    {
        $data = [

            "nomor_rapat" => $this->input->post('nomor_rapat', true),
            "judul_rapat" => $this->input->post('judul_rapat', true),
            "tanggal" => $this->input->post('tanggal', true),
            "nama_user" => $this->input->post('nama_user', true),
            "nama_ruang" => $this->input->post('nama_ruang', true),
            "jam_mulai" => $this->input->post('jam_mulai', true),
            "jam_selesai" => $this->input->post('jam_selesai', true),
            "hasil_record" => $this->input->post('hasil_record', false),
            "absen" => $this->input->post('absen', false)
        ];

        $this->db->where('id_notulen', $this->input->post('id_notulen'));
        $this->db->update('notulen', $data);
    }

    public function tambahDataNotulen()
    {
        $data = [

            "nomor_rapat" => $this->input->post('nomor_rapat', true),
            "judul_rapat" => $this->input->post('judul_rapat', true),
            "tanggal" => $this->input->post('tanggal', true),
            "nama_user" => $this->input->post('nama_user', true),
            "nama_ruang" => $this->input->post('nama_ruang', true),
            "jam_mulai" => $this->input->post('jam_mulai', true),
            "jam_selesai" => $this->input->post('jam_selesai', true),
            "hasil_record" => $this->input->post('hasil_record', false),
            "absen" => $this->input->post('absen', false)
        ];

        $this->db->where('id_notulen', $this->input->post('id_notulen'));
        $this->db->insert('notulen', $data);
    }
}
